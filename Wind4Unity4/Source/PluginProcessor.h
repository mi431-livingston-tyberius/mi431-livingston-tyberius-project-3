/*
  ==============================================================================
    THIS IS A HEADER FILE: THIS DEFINES DECLARATIONS OF STUFF (METHODS, VARS, ETC.)
    TO BE #INCLUDED IN .cpp FILES IN THE PROJECT. IT TELLS THE .cpp FILE THAT ALL OF
    THESE DECLARATIONS EXIST SO THAT IT IS ABLE TO USE ANYTHING LISTED HERE EVEN IF
    IT IS NOT DEFINED IN THE .cpp FILE ITSELF. IT WILL NOT KNOW THIS STUFF EXISTS
    OTHERWISE (unless you copy and paste the definition to every .cpp file that uses
    it)

    (because atm I'm only using 1 cpp file, this use doesn't entirely translate in this
    project. If I had another .cpp file that included this header file, it'd be able to
    reference/call any of this stuff even though they are defined in PluginProcessor.cpp)
  ==============================================================================
*/

#pragma once
/*
    ^ "pragma" Is a way to give instructions, info, or toggle of some features to the compiler.
    "pragma once" tells the compiler to only include this file once in a given "translation unit"
    (mainly a .cpp file). If this file is included twice in a .cpp file (or is included alongside
    another file that that also includes this file) and we DON'T write this, we'll get redefinition
    errors (we can only have one definition of stuff like structs & classes per "translation unit"
    as per C++'s One Definition Rule)
*/

#include <JuceHeader.h>
#include <random>

enum GustType
{
    None,
    Gust,
    Squall
};

// Struct to hold all plugin parameters
struct PluginSettings
{
    float gain { 0 }, dstAmplitude { 0 };
    float gustDepth { 0 }, gustInterval { 0 }, squallDepth { 0 };
    bool onToggle { false }, constantGust { false };
    int windForce { 3 };
    GustType gustType {None};
};

PluginSettings getPluginSettings(juce::AudioProcessorValueTreeState& apvts);

//==============================================================================
/**
*/
class Wind4Unity4AudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    Wind4Unity4AudioProcessor();
    ~Wind4Unity4AudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override { return false; };
    bool producesMidi() const override { return false; };
    bool isMidiEffect() const override { return false; };
    double getTailLengthSeconds() const override { return 0.0; };

    //==============================================================================
    int getNumPrograms() override { return 1; };
    int getCurrentProgram() override { return 0; };
    void setCurrentProgram (int index) override {} ;
    const juce::String getProgramName (int index) override {return {}; };
    void changeProgramName (int index, const juce::String& newName) override {};

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    // APVTS helps sync changes in the GUI to the vars in the DSP
    static juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout(); // APVTS requires we pass ALL parameters used in a layout. This will generate it
    juce::AudioProcessorValueTreeState apvts {*this, nullptr, "Parameters", createParameterLayout()};
    
    enum GustStatus
    {
        Off,
        Waiting,
        Active,
        Closing
    };

private:
    // Wind Methods
    void dstPrepare(const juce::dsp::ProcessSpec& spec);
    void dstProcess(juce::AudioBuffer<float>& buffer);
    void dstUpdateSettings();
    float randomNormal();
    void windSpeedSet();
    void computeGust();
    void gustSet();
    void squallSet();
    void gustClose();
    void gustIntervalSet();
    void gustLengthSet();
    void squallLengthSet();
    
    // Distant Wind DSP
    juce::dsp::Oscillator<float> distNoise1
    {[] (float x)
        { juce::Random r; return r.nextFloat() * 2.0f - 1.0f;}
    };
    
    juce::dsp::StateVariableTPTFilter<float> dstBPF;
    
    // RNG
    std::normal_distribution<double> distribution{};
    std::default_random_engine generator;
    
    // Wind Speed Arrays
    float meanWS[13] { 0.0f, 1.0f, 2.0f, 4.0f, 6.0f, 9.0f, 12.0f, 15.0f, 18.0f, 22.0f, 26.0f, 30.0f, 34.0f };
    float sdWS[13] { 0.0f, 0.125f, 0.25f, 0.5f, 0.75f, 1.125f, 1.5f, 1.875f, 2.25f, 2.75f, 3.75f, 4.25f };
    
    // Internal Vars
    juce::dsp::ProcessSpec currentSpec;
    float currentWindSpeed { 0.0f };
    float deltaWindSpeed { 0.0f };          // How much wind changes each frame
    float targetWindSpeed { 0.0f };         // speed we're aiming for, when hit - new target generated
    int currentWSComponentCounter { 0 };    // frames of current windspeed change
    int targetWSComponentCount { 0 };       // Total amt of frames for this WS change
    float currentGust { 0.0f };     // (Speed)
    float deltaGust { 0.0f };       // (Speed)
    float targetGust { 0.0f };      // (Speed)
    int currentGustComponentCounter { 0 };  // Component = Individual ups and downs of the gusts
    int targetGustComponentCount { 0 };
    int currentGustDurationCounter { 0 };     // Duration = Total duration of a gust event
    int targetGustDurationCount { 0 };
    int currentGustIntervalCounter { 0 };   // Intervals = gaps between the gusts
    int targetGustIntervalCount { 0 };
    GustStatus gustStatus;
    bool gustWasActive { false };
    PluginSettings pluginSettings;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Wind4Unity4AudioProcessor)
};
