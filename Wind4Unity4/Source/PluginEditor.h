/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

struct BasicRotarySlider : juce::Slider
{
    BasicRotarySlider() : juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag,
                                       juce::Slider::TextEntryBoxPosition::TextBoxBelow)
    {
        
    }
};

//==============================================================================
/**
*/
class Wind4Unity4AudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    Wind4Unity4AudioProcessorEditor (Wind4Unity4AudioProcessor&);
    ~Wind4Unity4AudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Wind4Unity4AudioProcessor& audioProcessor;
    
    // Groups
    juce::GroupComponent gustGroup, windGroup, utilityGroup;
    
    // Utility
    juce::ToggleButton onToggle {"Play Wind"};
    juce::Slider gain;
    juce::Label gainLabel;
    
    // Wind
    juce::Slider windForce;
    BasicRotarySlider dstAmp;
    juce::Label windForceLabel;
    juce::Label dstAmpLabel;
    
    // Gust
    BasicRotarySlider gustDepth;
    BasicRotarySlider gustInterval;
    juce::ComboBox gustType;
    juce::ToggleButton constantGustToggle {"Constant Wind"};
    
    juce::Label gustDepthLabel;
    juce::Label gustIntervalLabel;
    juce::Label gustTypeLabel;
    
    // Attaching parameters to the sliders
    using APVTS = juce::AudioProcessorValueTreeState; // using = pretty much makes the left a shortcut for the right
    using SliderAttachment = APVTS::SliderAttachment;
    
    SliderAttachment gainSliderAttachment,
                     windForceSliderAttachment,
                     dstAmpSliderAttachment,
                     gustDepthSliderAttachment,
                     gustIntervalSliderAttachment;
    
    APVTS::ButtonAttachment onToggleAttachment,
                            constantGustToggleAttachment;
    
    APVTS::ComboBoxAttachment gustTypeComboBoxAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Wind4Unity4AudioProcessorEditor)
};
