/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Wind4Unity4AudioProcessor::Wind4Unity4AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                       .withOutput ("Output", 
                            juce::AudioChannelSet::stereo(), true)
                       )
#endif
{
    // Seed RNG
    int seed = int(std::chrono::system_clock::now().time_since_epoch().count());
    generator.seed(seed);
    
    // Set Initial Wind Speed
    windSpeedSet();
}

Wind4Unity4AudioProcessor::~Wind4Unity4AudioProcessor()
{
}

//==============================================================================
const juce::String Wind4Unity4AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

//==============================================================================
// CALLED BEFORE PLAYBACK STARTS TO LET PROCESSOR PREP ITSELF
void Wind4Unity4AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Create DSP Spec
    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = getTotalNumOutputChannels();
    spec.sampleRate = sampleRate;
    currentSpec = spec;
    
    pluginSettings = getPluginSettings(apvts);
    
    dstPrepare(spec);
}

void Wind4Unity4AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Wind4Unity4AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

// RENDERS NEXT BUFFER BLOCK //
void Wind4Unity4AudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    
    // Clear buffer & update parameters
    buffer.clear();
    pluginSettings = getPluginSettings(apvts);
    
    // Skip processing if off
    if (!pluginSettings.onToggle) return;
    
    // Wind speed
    if (++currentWSComponentCounter > targetWSComponentCount)
        windSpeedSet();
    else
        currentWindSpeed += deltaWindSpeed;
    
    // Gust
    if (pluginSettings.gustType == GustType::Gust || pluginSettings.gustType == GustType::Squall || gustStatus == Closing)
    {
        computeGust();
    }
    
    else if (gustStatus == Active)
    {
        // Not activated, close the boi
        gustClose();
    }
    
    // ... Gust stuff above...
    
    // Update + process dst & add to buffer
    dstUpdateSettings();
    dstProcess(buffer);
    
    buffer.applyGain(pluginSettings.gain);
}

//==============================================================================
bool Wind4Unity4AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* Wind4Unity4AudioProcessor::createEditor()
{
    return new Wind4Unity4AudioProcessorEditor(*this);
    //return new juce::GenericAudioProcessorEditor(*this);
}

//==============================================================================
void Wind4Unity4AudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    
    juce::MemoryOutputStream mos (destData, true);
    apvts.state.writeToStream(mos);
}

void Wind4Unity4AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    auto tree = juce::ValueTree::readFromData(data, sizeInBytes);
    if (tree.isValid())
    {
        apvts.replaceState(tree);
        pluginSettings = getPluginSettings(apvts);
    }
}

PluginSettings getPluginSettings(juce::AudioProcessorValueTreeState& apvts)
{
    PluginSettings settings;
    
    // Global
    settings.gain = apvts.getRawParameterValue("Master Gain") -> load();
    settings.onToggle = apvts.getParameter("On/Off Toggle") -> getValue();
    
    // Dst
    settings.dstAmplitude = apvts.getRawParameterValue("DstAmp") -> load();
    
    // Wind Speed bois
    settings.windForce = apvts.getRawParameterValue("Wind Force") -> load();
    settings.constantGust = apvts.getParameter("Constant Gust") -> getValue();
    
    settings.gustDepth = apvts.getRawParameterValue("Gust Depth") -> load();
    settings.gustInterval = apvts.getRawParameterValue("Gust Interval") -> load();
    
    //settings.squallDepth = apvts.getRawParameterValue("Squall Depth") -> load();
    
    settings.gustType = static_cast<GustType>(apvts.getRawParameterValue("Gust Type") -> load());
    
    return settings;
}

juce::AudioProcessorValueTreeState::ParameterLayout Wind4Unity4AudioProcessor::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;
    
    // Normalizable range
    
    // Global params
    layout.add(std::make_unique<juce::AudioParameterFloat>(juce::ParameterID{"Master Gain", 1}, // ParamID (name and version ID) b/c AU bois like it better
                                                           "Master Gain",                       // Name of param
                                                           juce::NormalisableRange<float>(0.0f, 1.0f, 0.1f, 1.0f),  // Range (min, max, interval, skew) normalisable can swap bet. 0-1 and abs.
                                                           0.5f));                                                  // Default
    layout.add(std::make_unique<juce::AudioParameterBool>(juce::ParameterID{"On/Off Toggle", 1}, "On/Off Toggle", false));
    
    // Dst param
    layout.add(std::make_unique<juce::AudioParameterFloat>(juce::ParameterID{"DstAmp", 1}, "DistantAmpltd", juce::NormalisableRange<float>(0.0001f, 1.5f, 0.01f, 1.0f), 0.75f));
    
    // Wind Speed Params
    layout.add(std::make_unique<juce::AudioParameterInt>(juce::ParameterID{"Wind Force", 1}, "Wind Force", 0, 12, 3));
    layout.add(std::make_unique<juce::AudioParameterFloat>(juce::ParameterID{"Gust Depth", 1}, "Gust Depth", juce::NormalisableRange<float>(0.0f, 1.0f, 0.01f, 1.0f), 0.5f));
    layout.add(std::make_unique<juce::AudioParameterFloat>(juce::ParameterID{"Gust Interval", 1}, "Gust Interval", juce::NormalisableRange<float>(0.0f, 1.0f, 0.01f, 1.0f), 0.5f));
    layout.add(std::make_unique<juce::AudioParameterFloat>(juce::ParameterID{"Squall Depth", 1}, "Squall Depth", juce::NormalisableRange<float>(0.0f, 1.0f, 0.01f, 1), 0.5f));
    layout.add(std::make_unique<juce::AudioParameterBool>(juce::ParameterID{"Constant Gust", 1}, "Constant Gust", false));
    
    // Parameter Choice! can take string array and make it into a dropdown in the layout
    juce::StringArray stringArray{"None", "Gust", "Squall"};
    layout.add(std::make_unique<juce::AudioParameterChoice>(juce::ParameterID{"Gust Type", 1}, "Gust Type", stringArray, 0));

    return layout;
}

void Wind4Unity4AudioProcessor::dstPrepare(const juce::dsp::ProcessSpec &spec) {
    // Prepare Noise Source
    distNoise1.prepare(spec);
    
    // Prepare Filter
    dstBPF.prepare(spec);
    dstBPF.setType(juce::dsp::StateVariableTPTFilterType::bandpass);
    dstBPF.setCutoffFrequency(10.0f);
    dstBPF.setResonance(1.0f);
    dstBPF.reset();
}

void Wind4Unity4AudioProcessor::dstProcess(juce::AudioBuffer<float> &buffer) {
    // Get Buffer info
    int numSamples = buffer.getNumSamples();
    int numChannels = buffer.getNumChannels();
    float dstFrameAmp = pluginSettings.dstAmplitude;
    
    // Distant Wind DSP Loop (For every channel (and each sample w/in the frame on that channel) create a new bandpass filter noise)
    for (int ch = 0; ch < numChannels; ++ch) {
        for (int s = 0; s < numSamples; ++s) {
            float output = dstBPF.processSample(ch, distNoise1.processSample(0.0f));
            buffer.addSample(ch, s, output * dstFrameAmp);
        }
    }
}

void Wind4Unity4AudioProcessor::dstUpdateSettings() {
    if (currentWindSpeed < 0 )
        currentWindSpeed = 0;
        
    // Apply WS and gust amounts to the bandpass
    dstBPF.setCutoffFrequency(juce::jlimit(0.004f, 1500.0f,(currentWindSpeed + currentGust) * 30.0f));
    dstBPF.setResonance(1.0f + log(juce::jmax(1.0f, (currentWindSpeed + currentGust) * 0.1f)));
}

float Wind4Unity4AudioProcessor::randomNormal() {
    return distribution(generator);
}

void Wind4Unity4AudioProcessor::windSpeedSet() {
    int force = pluginSettings.windForce;
    
    if (force == 0)
    {
        targetWindSpeed = 0.0f;
        targetWSComponentCount = (int) (currentSpec.sampleRate / currentSpec.maximumBlockSize); // The sample rate / block size gives us frame amt
    }
    
    else
    {
        targetWindSpeed = meanWS[force] + sdWS[force] * randomNormal();
        targetWSComponentCount = (int) ((10.0f + 2.0f * randomNormal()) / (force / 2.0f) * currentSpec.sampleRate / currentSpec.maximumBlockSize);
    }
    
    currentWSComponentCounter = 0;
    deltaWindSpeed = (targetWindSpeed - currentWindSpeed) / (float) targetWSComponentCount;
}

void Wind4Unity4AudioProcessor::computeGust() {
    // If first time or coming back from being off, set interval
    if (!gustWasActive)
    {
        gustIntervalSet();
        return;
    }
    
    if (pluginSettings.constantGust)
    {
        targetGustIntervalCount = 0;
    }
    
    // If waiting, if end of interval set gust/squall component and length
    if (gustStatus == Waiting)
    {
        if (++currentGustIntervalCounter > targetGustIntervalCount)
        {
            if (pluginSettings.gustType == GustType::Squall)
            {
                squallSet();
                squallLengthSet();
            }
            
            else
            {
                gustSet();
                gustLengthSet();
            }
        }
        
        return;
    }
    
    // If Active, Close if at end of length, set new gust/squall component if at
    //    end of component, else increment gust speed
    if (gustStatus == Active)
    {
        // Close gust after it's duration finishes
        if (++currentGustDurationCounter > targetGustDurationCount)
        {
            // Don't close the gust if we want it constant
            if (!pluginSettings.constantGust)
            {
                gustClose();
                return;
            }
        }
        
        if (++currentGustComponentCounter > targetGustComponentCount)
        {
            if (pluginSettings.gustType == GustType::Squall)
            {
                squallSet();
            }
            
            else
            {
                gustSet();
            }
        }
        
        else
        {
            currentGust += deltaGust;
        }
        
        return;
    }
    
    // If Closing and end of component set gust speed zero, set wait length and return
    //    else increment gust speed
    if (gustStatus == Closing)
    {
        if (++currentGustComponentCounter > targetGustComponentCount)
        {
            gustWasActive = false;
            gustStatus = Off;
            currentGust = 0.0f;
            return;
        }
        
        else
        {
            currentGust += deltaGust;
        }
    }
}

void Wind4Unity4AudioProcessor::gustSet() {
    int force = pluginSettings.windForce;
    if (force < 3)
    {
        gustClose();
        return;
    }
    
    else
    {
        // Find our new depth and duration for this next gust
        targetGust = 15.0f * pluginSettings.gustDepth + 2.0f * sdWS[force] * randomNormal();
        targetGustComponentCount = (int) ((1.0f + 0.25f * randomNormal()) / (force / 2.0f) * currentSpec.sampleRate / currentSpec.maximumBlockSize);
        juce::jmax(targetGustComponentCount, 1); // Possible for us to get a negative number w/ ^. Make sure that doesn't happen
    }
    
    currentGustComponentCounter = 0;
    deltaGust = (targetGust - currentGust) / (float) targetGustComponentCount;
    gustStatus = Active;
}

void Wind4Unity4AudioProcessor::squallSet() {
    int force = pluginSettings.windForce;
    if (force < 3)
    {
        gustClose();
        return;
    }
    
    else
    {
        // Find our new depth and duration for this next squall
        targetGust = 20.0f * pluginSettings.gustDepth + 2.0f * sdWS[force] * randomNormal();
        targetGustComponentCount = (int) ((0.75f + 0.25f * randomNormal()) / (force / 2.0f) * currentSpec.sampleRate / currentSpec.maximumBlockSize);
        juce::jmax(targetGustComponentCount, 1); // Possible for us to get a negative number w/ ^. Make sure that doesn't happen
    }
    
    currentGustComponentCounter = 0;
    deltaGust = (targetGust - currentGust) / (float) targetGustComponentCount;
    
    
    gustStatus = Active;
}

void Wind4Unity4AudioProcessor::gustClose() {
    targetGust = 0.0f;
    targetGustComponentCount = (int) (2 * currentSpec.sampleRate / currentSpec.maximumBlockSize);
    currentGustComponentCounter = 0;
    deltaGust = (targetGust - currentGust) / (float) targetGustComponentCount;
    gustStatus = Closing;
}

void Wind4Unity4AudioProcessor::gustIntervalSet() {
    currentGustIntervalCounter = 0;
    
    // Set next interval only if random gust
    if (!pluginSettings.constantGust)
    {
        targetGustIntervalCount = (int) ((5.0f + pluginSettings.gustInterval * 100.0f + 10.0f * randomNormal()) * currentSpec.sampleRate / currentSpec.maximumBlockSize);
        juce::jmax(targetGustIntervalCount, 1);
    }
    
    else
    {
        targetGustIntervalCount = 0;
    }
    
    gustStatus = Waiting;
    gustWasActive = true;
}

void Wind4Unity4AudioProcessor::gustLengthSet() {
    currentGustDurationCounter = 0;
    targetGustDurationCount = (int) ((5.0f + 1.5f * randomNormal()) * currentSpec.sampleRate / currentSpec.maximumBlockSize);
    juce::jmax(targetGustDurationCount, 1);
    gustStatus = Active;
}

void Wind4Unity4AudioProcessor::squallLengthSet() {
    currentGustDurationCounter = 0;
    targetGustDurationCount = (int) (((30.0f + 5.0f * randomNormal())) * currentSpec.sampleRate / currentSpec.maximumBlockSize);
    juce::jmax(targetGustDurationCount, 1);
    gustStatus = Active;
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Wind4Unity4AudioProcessor();
}
