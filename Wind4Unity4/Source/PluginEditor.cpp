/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Wind4Unity4AudioProcessorEditor::Wind4Unity4AudioProcessorEditor (Wind4Unity4AudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p),

gainSliderAttachment(audioProcessor.apvts, "Master Gain", gain),
windForceSliderAttachment(audioProcessor.apvts, "Wind Force", windForce),
dstAmpSliderAttachment(audioProcessor.apvts, "DstAmp", dstAmp),
gustDepthSliderAttachment(audioProcessor.apvts, "Gust Depth", gustDepth),
gustIntervalSliderAttachment(audioProcessor.apvts, "Gust Interval", gustInterval),
onToggleAttachment(audioProcessor.apvts, "On/Off Toggle", onToggle),
constantGustToggleAttachment(audioProcessor.apvts, "Constant Gust", constantGustToggle),
gustTypeComboBoxAttachment(audioProcessor.apvts, "Gust Type", gustType)

{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    // Mainly for component init.
    
    // Borders/Groups
    addAndMakeVisible(utilityGroup);
    addAndMakeVisible(windGroup);
    addAndMakeVisible(gustGroup);
    
    // Set group titles
    utilityGroup.setText("Utility");
    windGroup.setText("Wind");
    gustGroup.setText("Gusts");
    
    // Utility
    addAndMakeVisible(onToggle);
    addAndMakeVisible(gain);
    addAndMakeVisible(gainLabel);
    
    gain.setSliderStyle(juce::Slider::LinearHorizontal);
    gain.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 50, 15);
    gainLabel.setText("Gain", juce::dontSendNotification);
    
    // Wind
    addAndMakeVisible(windForce);
    addAndMakeVisible(dstAmp);
    addAndMakeVisible(windForceLabel);
    addAndMakeVisible(dstAmpLabel);
    
    windForce.setSliderStyle(juce::Slider::IncDecButtons);
    windForce.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 80, 120);
    
    windForceLabel.setText("Wind Force", juce::dontSendNotification);
    dstAmpLabel.setText("Distance Amplitude", juce::dontSendNotification);
    
    // Gust
    addAndMakeVisible(gustDepth);
    addAndMakeVisible(gustInterval);
    addAndMakeVisible(gustType);
    addAndMakeVisible(constantGustToggle);
    
    gustType.setEditableText(false);
    gustType.addItem("None", 1);
    gustType.addItem("Gust", 2);
    gustType.addItem("Squall", 3);
    gustType.setSelectedItemIndex(0);
    gustType.setJustificationType(juce::Justification(36));
    
    addAndMakeVisible(gustDepthLabel);
    addAndMakeVisible(gustIntervalLabel);
    addAndMakeVisible(gustTypeLabel);
    gustTypeLabel.setText("Gust Type", juce::dontSendNotification);
    gustDepthLabel.setText("Gust Depth", juce::dontSendNotification);
    gustIntervalLabel.setText("Gust Interval", juce::dontSendNotification);
    
    
    
    setSize (600, 600);
}

Wind4Unity4AudioProcessorEditor::~Wind4Unity4AudioProcessorEditor()
{
}

//==============================================================================
void Wind4Unity4AudioProcessorEditor::paint (juce::Graphics& g)
{
    // Gives access to the Graphics class which can make stuff look prettyyyy.
    // All cool drawing ends up here
    g.fillAll (juce::Colours::black.brighter(0.1f));

    g.setColour (juce::Colours::whitesmoke);
    g.setFont (36.0f);
    g.drawFittedText ("Wind Zone :)", getLocalBounds(), juce::Justification::centredTop , 1);
    
    g.setColour(juce::Colours::gold);
    g.drawLine(0, 45, getWidth(), 45, 2);
}

void Wind4Unity4AudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    // All positioningm sizing and setup, usually
    
    // Margins
    auto leftMargin = getWidth() * 0.02f;
    auto topMargin = getHeight() * 0.10f;
    
    auto groupWidth = getWidth() * 0.965f;
    auto utilityGroupHeight = getHeight() * 0.16;
    auto windGroupHeight = getHeight() * 0.3f;
    auto gustGroupHeight = getHeight() * 0.39f;
    auto rotarySliderSize = 120;
    
    // Group sizes
    utilityGroup.setBounds(leftMargin, topMargin, groupWidth, utilityGroupHeight);
    windGroup.setBounds(leftMargin, utilityGroup.getBottom() + 10, groupWidth, windGroupHeight);
    gustGroup.setBounds(leftMargin, windGroup.getBottom() + 10, groupWidth, gustGroupHeight);
    
    // Utility
    onToggle.setBounds(leftMargin + 30, topMargin + 15, 100, 50);
    gain.setBounds(getWidth() * 0.40f, onToggle.getY(), 300, 50);
    gainLabel.setBounds(gain.getX() + (gain.getWidth() / 2 - 5), gain.getY() + 30, 80, 30);
    
    // Wind
    dstAmp.setBounds(getWidth() * 0.66f, 190, rotarySliderSize, rotarySliderSize);
    windForce.setBounds(getWidth() * 0.16f, 190, rotarySliderSize, rotarySliderSize);
    dstAmpLabel.setBounds(dstAmp.getX(), dstAmp.getY() + 120, 130, 30);
    windForceLabel.setBounds(windForce.getX() + 7, windForce.getY() + 120, 130, 30);
    
    // Gust
    gustDepth.setBounds(windForce.getX() - 15, windGroup.getBottom() + 20, rotarySliderSize, rotarySliderSize);
    gustInterval.setBounds(dstAmp.getX(), windGroup.getBottom() + 20, rotarySliderSize, rotarySliderSize);
    gustDepthLabel.setBounds(gustDepth.getX() + 21, gustDepth.getY() + 120, 130, 30);
    gustIntervalLabel.setBounds(gustInterval.getX() + 17, gustInterval.getY() + 120, 130, 30);
    
    gustType.setBounds(gustDepth.getX() - 15, gustDepth.getBottom() + 48, 175, 20);
    constantGustToggle.setBounds(gustInterval.getX() - 6, gustInterval.getBottom() + 46, 120, 30);
    gustTypeLabel.setBounds(gustType.getX() + 42, gustType.getY() + 19, 130, 30);
}
